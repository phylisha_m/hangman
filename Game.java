import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Game extends Player {

    static Player p;
    static Board b;
    static int wrongGuesses = 0;
    static String usedGuesses = "";
    public static String missed = "";


    //create the secret word
    public static String secretWord() {

        String secret = List.of("fin", "jam", "pin", "con", "why", "fog", "ton", "fun", "ear").get(ThreadLocalRandom.current().nextInt(0,8));
        // System.out.println(secret);
        return secret;
    }


    public static String missedLetters(String secret, String userGuess) {

        if(userGuess.length() == 3){
            for(int i = 0; i<secret.length()-1; i++) {


                if (secret.charAt(i) == userGuess.charAt(i)) {

                } else {

                    missed += userGuess.charAt(i);
                }
            }
        }
        else{
            if(secret.contains(userGuess)){

            }
            else{
                missed+= userGuess;
            }
        }
        System.out.println();
        System.out.println("Missed Letters: " + missed);
        return missed;
    }


    //number of wrong guesses
    public static int wrongGuesses(String display, String userGuess) {

        if (!display.contains(userGuess)) {
            wrongGuesses++;
            System.out.println("Wrong...");
            System.out.println();
        }


        return wrongGuesses;
    }

    //displays what letters have been guessed already
    public static String alreadyGuessed(String userGuess) {
        if (usedGuesses.contains(userGuess)) {
            System.out.println("\n" + "You already guessed this letter.." + "\n");

        } else {
            usedGuesses += userGuess;

        }
        return usedGuesses;
    }


    public static void playGame() {

        int guesses = 0;
        boolean looping= true;
        String display;
        String secret = secretWord();
        //boolean PLAY = true;
        String mystery;
        p = new Player();
        b = new Board();
        System.out.println(" H A N G M A N");
        System.out.println(secret);


        while (looping) {

            System.out.println(b.displayBoard(wrongGuesses));


            String userGuess = p.makeGuess();
            //ecret(secret, userGuess);
            mystery = b.displaySecret(secret, userGuess);
            missedLetters(secret, userGuess);
            alreadyGuessed(userGuess);
            System.out.println(b.displaySecret(secret, userGuess));
            wrongGuesses(b.displaySecret(secret,userGuess), userGuess);

            if(!mystery.contains("_")) {
                System.out.println();
                System.out.println("You guessed it!");
                looping = false;
            }
            else if(wrongGuesses >3){
                System.out.println();
                System.out.println("You Lose... so sad :( ");
                looping = false;
            }


            }


    }


}






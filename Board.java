public class Board extends Game {

    public static String displayBoard(int wrongGuesses){

        return switch(wrongGuesses){

            case 0 -> """
                    
                    +-+
                     |
                     |
                     |
                     ===
                     ___
                    """;
            case 1 -> """
                    
                    +-+
                    0|
                     |
                     |
                     ===
                    """;
            case 2 -> """
                    
                    +-+
                    0|
                    ||
                     |
                     ===
                    """;
            case 3 -> """
                    
                    +-+
                    0|
                    ||
                    ||
                     ===
                    """;


            default -> throw new IllegalStateException("Unexpected value: " + wrongGuesses);
        };

    }


    public static String displaySecret(String secret, String userGuess){
        //StringBuilder display = new StringBuilder();
        var display = "";
        // guesses = 0;
        for(var letter: secret.split("")){
            if(userGuess.contains(letter)){
                display+= letter;

            }
            else{
                display+="_";

            }

        }

        return display;
    }


}

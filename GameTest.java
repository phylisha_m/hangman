import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {


   

    @Test
    void alreadyGuessed() {
        Game g = new Game();
       String usedGuesses = "";
       assertEquals("j", g.alreadyGuessed("j"));
        assertEquals("jja", g.alreadyGuessed("ja"));
        assertEquals("jjaham", g.alreadyGuessed("ham"));
    }

    @Test
    void secretWord() {
        Game g = new Game();
        Board b = new Board();
        assertEquals("___", b.displaySecret("ton","jam"));
        assertEquals("t__", b.displaySecret("ton","tip"));
        assertEquals("to_", b.displaySecret("ton","top"));
        assertEquals("t_n", b.displaySecret("ton","tin"));
        assertEquals("ton", b.displaySecret("ton","ton"));
    }
}
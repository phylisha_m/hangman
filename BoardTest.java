import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void displayBoard() {
        Board b = new Board();
        Game g = new Game();
        assertEquals("""
                    
                    +-+
                     |
                     |
                     |
                     ===
                     ___
                    """, b.displayBoard(0));
        assertEquals("""
                    
                    +-+
                    0|
                     |
                     |
                     ===
                    """, b.displayBoard(1));
        assertEquals("""
                    
                    +-+
                    0|
                    ||
                     |
                     ===
                    """, b.displayBoard(2));
        assertEquals("""
                    
                    +-+
                    0|
                    ||
                    ||
                     ===
                    """, b.displayBoard(3));
    }

    @Test
    void displaySecret() {
        Game g = new Game();
        Board b = new Board();
        assertEquals("___", b.displaySecret("ton","jam"));
        assertEquals("t__", b.displaySecret("ton","tip"));
        assertEquals("to_", b.displaySecret("ton","top"));
        assertEquals("t_n", b.displaySecret("ton","tin"));
        assertEquals("ton", b.displaySecret("ton","ton"));
    }
}